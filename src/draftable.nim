# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import std/os as os
import std/parseopt as po
from std/paths import Path, `==`
from std/strformat import fmt
from std/times import Time, format, initDuration, `-`, `<`

# dependencies
import webui as webui

# submodule imports
when defined(DEBUG):
    import debug

# project imports
import version as version
import config as config
import project_paths as project_paths
import table as table
import exports as exports


const
    OptionsLongNoVal = @[
        "help",
        "version",
    ]

    ProjectConfigFilename = "project.draftable"
    UpdateSleepMs = 1000

    IconData = staticRead("../icons/favicon.svg")
    IconType = "image/svg+xml"

    HeadJs = staticRead("../templates/resources/script.js")
    HeadCssReadableCss = staticRead("../templates/resources/readable.css")
    HeadCss = staticRead("../templates/resources/style.css")
    BaseTemplateStartStub = staticRead("../templates/resources/base-start.html")
    BaseTemplateStart = fmt(BaseTemplateStartStub)
    BaseTemplateEnd = staticRead("../templates/resources/base-end.html")

    PageIndex = "index"
    PageIndexTemplate = staticRead(fmt"../templates/{PageIndex}.html")
    PageLicenses = "licenses"
    PageLicensesTemplate = staticRead(fmt"../templates/{PageLicenses}.html")

when defined(DEBUG):
    const
        TimeFormat = "HH:mm:ss"


type
    Status = object
        current_page: string
        modified_old: Time
        modified_new: Time
        is_outdated: bool = true


proc show_help =
    echo(version.long())
    echo(version.compiled())
    echo(version.copyright())
    echo()
    echo(fmt"    {version.ProgramName} [options] PROJECT_DIR")
    echo()
    echo("Options for direct output:")
    echo("  --help         WARNING! Show this help and exit")
    echo("  --version      WARNING! Show version information and exit")
    quit(QuitSuccess)


proc eh_click_hello(e: webui.Event): string =
    let js_fn_name: string = e.element
    echo("JS function call to '", js_fn_name, "', event: '", e.eventType, "'")
    return "Message from Nim"


proc closure_navigate_to_page(status: ref Status, page: string): proc =

    proc navigate_to_page(e: webui.Event): string =
        when defined(DEBUG):
                debug.print(fmt"Navigating to '{page}' ...")
        status.current_page = page
        status.is_outdated = true
        return fmt"Navigated to '{page}'"

    result = navigate_to_page


proc live_view(config: config.Config, main_file_path: Path): string =

    var
        status: ref Status
        template_table_inner: string
        template_filled: string
    when defined(DEBUG):
        var
            modified_old_s: string
            modified_new_s: string

    new(status)
    status.current_page = PageIndex
    status.modified_old = os.getLastModificationTime(main_file_path.string)
    status.modified_new = status.modified_old - initDuration(minutes=1)

    let window: webui.Window = webui.newWindow()
    window.setIcon(IconData, IconType)
    window.bind("eh_click_hello", eh_click_hello)
    window.bind("navigate_index", closure_navigate_to_page(status, PageIndex))
    window.bind("navigate_licenses", closure_navigate_to_page(status, PageLicenses))

    while true:
        if status.is_outdated:
            when defined(DEBUG):
                modified_old_s = status.modified_old.format(TimeFormat)
                modified_new_s = status.modified_new.format(TimeFormat)
                debug.print(fmt"main modified: '{modified_old_s}' -> '{modified_new_s}'")
            case status.current_page
                of PageLicenses:
                    template_filled = fmt(PageLicensesTemplate)
                else:
                    template_table_inner = table.build(main_file_path)
                    template_filled = fmt(PageIndexTemplate)
            window.show(template_filled)
            status.is_outdated = false

        if not window.shown():
            when defined(DEBUG):
                debug.print("Window not shown anymore, exiting...")
            break

        os.sleep(UpdateSleepMs)

        status.modified_old = status.modified_new
        status.modified_new = os.getLastModificationTime(main_file_path.string)
        status.is_outdated = status.is_outdated or status.modified_old < status.modified_new

    result = fmt(PageIndexTemplate)


proc main =

    var p = po.initOptParser(shortNoVal = {}, longNoVal = OptionsLongNoVal)

    when defined(DEBUG):
        var p_debug = p
        debug.output_options(p_debug)

    while true:
        p.next()
        case p.kind
            of po.cmdEnd:
                break
            of po.cmdShortOption:
                quit(fmt"This program does not take short arguments: '{p.key}'", QuitFailure)
            of po.cmdLongOption:
                if p.key in OptionsLongNoVal and p.val != "":
                    quit(fmt"Command line option '{p.key}' doesn't take a value", QuitFailure)
                case p.key:
                # Options for direct output:
                    of "help":
                        show_help()
                    of "version":
                        quit(version.long(), QuitSuccess)
                    else:
                        quit(fmt"Unrecognized command line option '{p.key}'", QuitFailure)
            of po.cmdArgument:
                try:
                    project_paths.project_dir_load(p.key)
                except project_paths.ProjectDirAlreadySetError as e:
                    echo(e.msg)
                    quit(fmt"This program only takes one non-option argument - new: '{p.key}'", QuitFailure)
                except project_paths.ProjectDirValueError as e:
                    quit(e.msg, QuitFailure)

    var project_config_path: Path
    try:
        project_config_path = project_paths.load_file(ProjectConfigFilename)
    except project_paths.ProjectFileDoesNotExistError:
        quit(fmt"Project config file does not exist: '{ProjectConfigFilename}'", QuitFailure)

    let config: config.Config = config.parse_config(project_config_path)

    var main_file_path: Path
    try:
        main_file_path = project_paths.load_file(config.main_file)
    except project_paths.ProjectFileDoesNotExistError:
        quit(fmt"Project main file does not exist: '{config.main_file}'", QuitFailure)

    let last_rendered = live_view(config, main_file_path)

    if config.save_html:
        exports.html(last_rendered)


when isMainModule:
    main()
