# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import std/os as os
from std/paths import Path, `/`, `==`
from std/strformat import fmt


type
    ProjectDirAlreadySetError* = object of ValueError
    ProjectDirValueError* = object of ValueError
    ProjectFileDoesNotExistError* = object of OSError


const
    Empty*: Path = "".Path


var
    project_dir: Path = Empty


proc is_empty(check_path: Path): bool =
    result = (check_path.string == Empty.string)


proc project_dir_load*(arg_project_dir: string)
        {.raises: [ProjectDirAlreadySetError, ProjectDirValueError].} =

    if not is_empty(project_dir):
        try:
            raise newException(ProjectDirAlreadySetError, fmt"Project dir already set: '{project_dir.string}'")
        except ValueError:
            raise newException(ProjectDirAlreadySetError, fmt"Project dir already set: [FORMATTING ERROR]")
    elif arg_project_dir == "":
        raise newException(ProjectDirValueError, "No project dir provided")
    elif os.dirExists(arg_project_dir):
        project_dir = arg_project_dir.Path
    elif os.fileExists(arg_project_dir):
        raise newException(ProjectDirValueError, "Provided project dir is actually a file")
    else:
        raise newException(ProjectDirValueError, "Provided project dir does not exist")


proc create*(filename: string): Path =
    result = project_dir / filename.Path


proc load_file*(filename: string): Path
        {.raises: [ProjectFileDoesNotExistError].} =

    result = create(filename)

    if not os.fileExists(result.string):
        raise newException(ProjectFileDoesNotExistError, "")
