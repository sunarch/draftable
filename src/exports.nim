# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from std/paths import Path
from std/strformat import fmt

# submodule imports
when defined(DEBUG):
    import debug

# project imports
import project_paths as project_paths


proc html*(content: string) =
    let save_file_path: Path = project_paths.create("index.html")
    writeFile(save_file_path.string, content)
    when defined(DEBUG):
        debug.print(fmt"Saved HTML output to '{save_file_path.string}'")
